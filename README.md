[Version]:13
[Grid]:grid.kitely.com:8002:The%20Collective
[comment]:Added%20a%20Comment%20Function!
[Url]:https://bitbucket.org/thecollectivesl/tc-update-checker
[end]:end
# The Collective Update Checker #

This is a simple updater for your scripts to check if they are the current version

### Features ###

* Uses the Markdown to keep data on updates that does not rely on local prims to be avilable
* Parses a string into a list of [Version Number, Grid ID, Verson Comment Message]

### Who do I talk to? ###

* [Kurtis Anatine](https://www.kitely.com/forums/memberlist.php?mode=viewprofile&u=4102)